var numberOfColl = 6;
var numberOfRow = 10;

/**
*@Author: RienCus
* Not so random, credit to Kevin Valiant
*/
function createRandom() {
    var ddQuestion = document.getElementById("jumlahSoal");
    var numberOfQuestion = parseInt(
        ddQuestion.options[ddQuestion.selectedIndex].value);
    var questionBefore, question;
    
    for(var collNum=1; collNum<=numberOfColl; collNum++) {
        for(var rowNum=1; rowNum<=numberOfRow; rowNum++) {
            if (rowNum===1 || numberOfQuestion===1) 
                questionBefore=0;
                
            var pcId = "pc-" + collNum + "-" + rowNum;
            var pc = document.getElementById(pcId);
            
            do{
                question = Math.floor((Math.random() * 10) % numberOfQuestion) +1;
            } while(question===questionBefore);
            questionBefore = question;
            
            pc.innerHTML = Math.floor(question);
        }
    }
}